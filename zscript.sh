#!/bin/bash

sudo pacman -Syu &&
sudo pacman -S git base-devel &&
git clone https://aur.archlinux.org/yay.git &&
cd yay &&
makepkg -si &&
yay -Syu
yay -S discord minecraft-launcher iridium-deb i3-gaps-next picom termite neovim